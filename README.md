# Simple Mock Server

This is just a simple mock server with 2 purposes:
- Analise the Request in server terminal
- Response with a mock
  


If you need several endpoints with specific mock responses, consider use the [npm-mockserver](https://www.npmjs.com/package/mockserver) instead

# Run
`npm run up`


# Usage
Request: **Please change the request to math your use case.**
```bash
curl --request PUT 'http://localhost:3000/rest/api/v1/client/' \
--header 'Content-Type: application/json' \
--data-raw '{
    "clientId":2404245,
    "age": 37,
    "foo":"bar"
}'
```

Request analizer at console log
```bash
Listening at http://localhost:3000
2021-09-22T14:38:14.387Z------------------------------------------------------------
 
Request Path:  /rest/api/v1/client/ 
Request Method:  PUT 
Request Body:
 {
  "clientId": 2404245,
  "age": 37,
  "foo": "bar"
} 

Sending response: 200
 {
  "clientId": 123,
  "name": 456,
  "transactionType": "Update Profile"
}
```

Response: **Please change the response to math your use case.**
```bash
#200 - OK
{
  "clientId": 123,
  "name": 456,
  "transactionType": "Update Profile"
}
```