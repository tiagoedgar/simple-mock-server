var express = require('express')
var bodyParser = require('body-parser')


// SETUP SERVER
const port = 3333;

const requestHandler = (req, res) => {

   const responseBody = {
      path: req.url,
      method: req.method,
      headers: req.headers,
      query: req.query,
      body: req.body,
      date: new Date().toISOString()
   }

   const response = JSON.stringify(responseBody, null, 2);
   console.log(responseBody);

   res.setHeader('Cache-control', 'no-cache');
   res.setHeader('Content-Type','application/json; charset=utf-8')
   res.statusCode = 200;
   res.end(response+"\n");
}


var app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(requestHandler)
app.listen(port)
console.log(`Listening at http://localhost:${port}`)

